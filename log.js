const request = require('request-promise');

class Log {

    constructor() {
        this.start = new Date();
    }

    push(result, type) {
        //disable log
        return;
        this.end = new Date();
        return request({
            uri: process.env.LOG_URL,
            method: 'POST',
            json: true,
            body: {
                type: type || process.env.LOG_TYPE,
                time: this.end.getTime() - this.start.getTime(),
                result
            }
        })
            .then(() => console.info('Push log success'))
            .catch(error => console.error('Push log error', error));
    }

    success(type) {
        this.push('Success', type);
    }

    error(type) {
        this.push('Error', type);
    }
}

module.exports = Log;
