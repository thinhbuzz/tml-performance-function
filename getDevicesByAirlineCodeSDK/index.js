/**NODE SDK using
 * Collection compliancedemo2
 * Get devices by airline code
 * @method: GET
 * @params: airlineCode - a Airline Code
 * @return: Devices JSON Object
 */
const _ = require('lodash');
const DocumentFactory = require('../api/DocumentDBUtils');
const Constants = require('documentdb/lib/constants');
const documentdbclient = new DocumentFactory();
const Log = require('../log');

module.exports = function (context, req, callback) {
    const log = new Log();

    documentdbclient.getAllDevicesByAirlineCode(req.query, function (error, results, responseHeaders) {
        context.log('getDevicesByAirlineCodeSDK');
        log.success('getDevicesByAirlineCodeSDK-QueryDatabase');
        const resDevices = _.map(results, function (o) {
            const device = _.omit(o, ['_ts', '_attachments', '_etag', '_self', '_rid']);
            if (device.compliance !== undefined) {
                const lastPackArr = [];
                const lastPack = _.last(device.compliance);
                if (lastPack !== undefined) {
                    lastPackArr.push(lastPack);
                    device.compliance = lastPackArr;
                } else {
                    device.compliance = [];
                }
            } else {
                device.compliance = [];
            }
            return device;
        });

        log.success('getDevicesByAirlineCodeSDK-Done');
        if (callback) {
            callback(resDevices, responseHeaders[Constants.HttpHeaders.Continuation]);
        } else {
            context.res = {
                status: 200,
                body: resDevices
            };

            context.done();
        }
    });
};