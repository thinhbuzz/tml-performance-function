/**Used compliancedemo2
 * Get airline info by its code
 * @method: GET
 * @params: > airlineCode : a Airline Code
 * @return: > airline info JSON Object
 */
const _ = require('underscore');
const devicesByAirlineCode = require('../getDevicesByAirlineCodeSDK/index');
const Log = require('../log');
module.exports = function (context, req) {
    const log = new Log();
    let airlineInfo = {};
    const responseData = context.bindings.code;
    //Get Airline Info
    try {
        if (responseData) {
            airlineInfo = _.first(_.filter(responseData, (airlineSchema) => {
                return airlineSchema.notifications;
            }));

        } else throw 'Please pass an airline code on the query string or in the request body';

        log.success('getAirlineByCode-QueryDatabase');
        let complete;
        //GEt device by airline code and put into airlineInfo obj for client can get devices from airline info object
        devicesByAirlineCode(context, req, function (devices, nextPage) {
            context.log('getAirlineByCode');
            log.success('getAirlineByCode-getDevicesByAirlineCodeSDK');

            complete = new Date();

            if (devices) airlineInfo.devices = devices;
            airlineInfo.nextPage = nextPage;
            context.res = {
                status: 200, /* Defaults to 200 */
                body: airlineInfo
            };
            log.success('getAirlineByCode-Done');
            context.done();
        });
    } catch (err) {
        log.error('getAirlineByCode');
        throw 'ERROR:' + err;
    }
};
