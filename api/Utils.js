const config = require('./Config');
const collectionLink = '/dbs/' + config.databaseId + '/colls/' + config.collectionId.compliancecollection;
const Utils = function () {
    this.enableCrossPartitionQuery = true; //false
};

Utils.prototype = {
    //GET RESULTS OF QUERY
    queryDocuments: function (context, query, options) {
        //var collLink = context.dbLink + '/colls/' + collectionId;
        const opts = Object.assign(Object.create(Object.prototype), {
            enableCrossPartitionQuery: this.enableCrossPartitionQuery
        }, options);
        return context.client.queryDocuments(collectionLink, query, opts);
    },
    readDocuments: function (context, query, options) {
        //var collLink = context.dbLink + '/colls/' + collectionId;
        const opts = Object.assign(Object.create(Object.prototype), {
            enableCrossPartitionQuery: this.enableCrossPartitionQuery
        }, options);
        return context.client.readDocuments(collectionLink, opts);
    },

    //CREATE A DOCUMENT
    createDocument: function (context, documentContent, callback) {
        //var collLink = context.dbLink + '/colls/' + collectionId;
        context.client.createDocument(collectionLink, documentContent, function (err, documents) {
            if (err) callback(err, null);
            else callback(null, documents);
        });
    },

    //REPLACE A DOCUMENT
    replaceDocument: function (context, document_self, documentContent, callback) {
        context.client.replaceDocument(document_self, documentContent, function (err, documents) {
            if (err) callback(err, null);
            else callback(null, documents);
        });
    },

    //DELETE A DOCUMENT
    deleteDocument: function (context, documentId, callback) {
        const documentUrl = collectionLink + '/docs/' + documentId;
        context.client.deleteDocument(documentUrl, function (err, documents) {
            if (err) callback(err, null);
            else callback(null, documents);
        });
    }

};
module.exports = Utils;