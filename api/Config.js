module.exports = {
    'documentdb': {
        'host': process.env.db_host,
        'authKey': process.env.db_key
    },
    'databaseId': process.env.db_id,
    'collectionId': { //Maybe has more collections
        'compliancecollection': process.env.collectionName
    }
};