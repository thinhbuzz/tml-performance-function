const DocumentClient = require('documentdb').DocumentClient;
const Config = require('./Config');
const Utils = require('./Utils');
const _ = require('lodash');
const uuidv4 = require('uuid/v4');
const datetime = require('node-datetime');
const Constants = require('documentdb/lib/constants');

const DocumentDBUtils = function () {
    //this.host = Config.documentdb.host;
    //this.authKey = Config.documentdb.authKey;
    this.client = new DocumentClient(Config.documentdb.host, {'masterKey': Config.documentdb.authKey});

    this.databaseId = Config.databaseId; //compliancedemo
    this.collectionId = Config.collectionId.airline; //compliancedemo2
    this.dbLink = '/dbs/' + this.databaseId;
    //this.collLink = this.dbLink + '/colls/' + this.collectionId;
    this.utils = new Utils();
};

DocumentDBUtils.prototype = {
    //GET an airline Info
    getAnAirlineInfoByCode: function (airlineCode, callback) {
        const self = this;
        const querySpec = {
            query: 'SELECT * FROM c WHERE c.code=@code and c.notifications != null',
            parameters: [{
                name: '@code',
                value: airlineCode
            }]
        };

        self.utils.queryDocuments(self, querySpec, null).toArray(function (err, results) {
            (err) ? callback(err) : callback(null, _.head(results));
        });
    },

    //GET all devices by airline code
    getAllDevicesByAirlineCode: function (query, callback) {
        const self = this;
        const querySpec = {
            query: 'SELECT * FROM c WHERE c.code=@code and c.active=true',
            parameters: [{
                name: '@code',
                value: query.airlineCode
            }]
        };
        const options = {maxItemCount: (query.perPage < 0 || query.perPage > 100) ? 10 : query.perPage};
        if (query.nextPage) {
            options.continuation = query.nextPage;
        }
        let countPage = 1;
        if (query.page < 1 || countPage === query.page) {
            return self.utils.queryDocuments(self, querySpec, options).executeNext(callback);
        }

        return self.utils.queryDocuments(self, querySpec, options).executeNext(function (error, results, responseHeaders) {
            if (!results || !responseHeaders || !responseHeaders[Constants.HttpHeaders.Continuation]) {
                return callback(error, results, responseHeaders);
            }
            return self.getAllDevicesByAirlineCode({
                airlineCode: query.airlineCode,
                page: query.page,
                nextPage: responseHeaders[Constants.HttpHeaders.Continuation],
                perPage: options.maxItemCount
            });
        });
    },

    //Get all history by airline code
    getAllHistoryByAirlineCode: function (airlineCode, callback) {
        const self = this;
        const querySpec = {
            query: 'SELECT * FROM c WHERE c.code=@code and c.pushed_history != null',
            parameters: [{
                name: '@code',
                value: airlineCode
            }]
        };
        //Query executed
        self.utils.queryDocuments(self, querySpec, null).toArray(function (err, results) {
            (err) ? callback(err, null) : callback(null, results);
        });
    },

    //Add a Push notification
    addAPushNotificationSDK: function (airlineCode, documentContent, callback) {
        const self = this;
        //Find document and update it
        self.getAllHistoryByAirlineCode(airlineCode, function (err, doc) {
            if (err) callback(err, null);

            if (doc.length !== 0) {//Has document let update it
                const prevPush = _.head(doc);
                //Update it
                prevPush.pushed_history.push(documentContent);
                self.utils.replaceDocument(self, prevPush._self, prevPush, function (err, results) {
                    (err) ? callback(err, null) : callback(null, results);
                });

            } else {
                //Create it with new info
                const createdDateDoc = datetime.create().format('Y-m-d H:M:S');
                const documentPush = {
                    'type': 'push notification history',
                    'id': uuidv4(),
                    'code': airlineCode,
                    'created_date': createdDateDoc,
                    'pushed_history': []
                };
                //Put content
                documentPush.pushed_history.push(documentContent);
                self.utils.createDocument(self, documentPush, function (err, results) {
                    (err) ? callback(err, null) : callback(null, results);
                });
            }
        });
    },

    //[TESTING] Add a device by airline code
    addADeviceByAirlineCodeTestingSDK: function (documentContent, callback) {
        const self = this;
        self.utils.createDocument(self, documentContent, function (err, results) {
            (err) ? callback(err, null) : callback(null, results);
        });
    },

    //GET all groups
    getAllGroupsByAirlinecode: function (airlineCode, callback) {
        const self = this;
        const querySpec = {
            query: 'SELECT c.groups from c WHERE c.code=@code and c.notifications != null',
            parameters: [{
                name: '@code',
                value: airlineCode
            }]
        };

        self.utils.queryDocuments(self, querySpec, null).toArray(function (err, results) {
            (err) ? callback(err, null) : callback(null, results);
        });
    },

    //TODO checking and add new Group
    addNewAGroupByAirlineCode: function (airlineCode, groupName, callback) {
        const self = this;
        //Find document => update its groupgroups
        self.getAnAirlineInfoByCode(airlineCode, function (err, airlineInfo) {
            if (err) callback(err);
            if (airlineInfo) {
                //Check if group existed or not
                if (!(_.indexOf(airlineInfo.groups, groupName) >= 0) && groupName !== '') {
                    //Add group here
                    airlineInfo.groups.push(groupName);
                    //Replace Document
                    self.utils.replaceDocument(self, airlineInfo._self, airlineInfo, function (err, results) {
                        (err) ? callback(err, null) : callback('success');
                    });
                } else {
                    //Callback No add group
                    callback(' Group name ' + groupName + ' is existed in airline ' + airlineCode + ' group or blank. It won\'t be added.');
                }
            }
        });
    }

};
module.exports = DocumentDBUtils;
