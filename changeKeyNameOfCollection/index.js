/**
 * Used compliancedemo2
 * Be careful when using this function, - this funciton not be use usually
 * Change key for whole data in collection , this function support for case change deviceOSId-> tml_id
 * @announce: + This function will effect to all document of typeDocument
 *            + Using exactly config template and value
 *            + Just for device/airline
 * @description: Ex: {"key":"value"} -> {"new_key":"value"}
 * @method: POST
 * @params: Object configuration
 * @return: status
 */
const _ = require('lodash');

module.exports = function (context, req) {
    try {
        const allData = context.bindings.inputDocumentIn;
        const config = req.body;
        const keyName = config.keyName;
        const expectedKeyName = config.expectedKeyName;
        let changedFieldData = [];

        //Detect data be changed
        if (config.typeDocument !== undefined && config.typeDocument === 'airline' || config.typeDocument === 'device') {
            if (config.typeDocument === 'airline') {
                context.log('----Change key name for Airline document type ---');
                changedFieldData = _.filter(allData, (airlineSchema) => {
                    return airlineSchema.notifications;
                });
            } else if (config.typeDocument === 'device') {
                context.log('----Change key name for Device document type ---');
                changedFieldData = _.filter(allData, (device) => {
                    return !device.notifications;
                });
            }

            //Start change field
            //context.log(changedFieldData);
            _.forEach(changedFieldData, function (data) {
                //DO SOME ACTION HERE FOR EACH DEVICE OR AIRLINE
                //data.device_created_date = data.datetime;

                /*if(data[keyName]=== 'null' || data[keyName] === null){
                    data[keyName] = "";
                }*/
                //delete data['deviceOSId'];
                //context.log(data[keyName]);
                //data[expectedKeyName] = data[keyName]; //Keep value for new field
                //delete data[keyName]; //Remove old field

                //Change key value for compliance "datetime" => reported datetime
                /*if(data.compliance || data.compliance.length !== ""){
                    _.forEach(data.compliance, function(baseline){
                        var baselineArr = baseline.baselines;
                             _.forEach(baselineArr, function(base){
                                  base.baseline.customer_code = base.baseline.airline_code;
                                  base.baseline.customer_name = base.baseline.airline_code + " AIRLINES";
                             });

                        //var timer = baseline.datetime;
                        //baseline[expectedKeyName] = timer;
                        //delete baseline.datetime;
                        //context.log(baselineArr);
                    });
                }*/

                //Add new compliance field statistic in document
                if (data.compliance != undefined && data.compliance.length > 0) {
                    const lastPackage = _.last(data.compliance);
                    if (lastPackage) {
                        const baselines = lastPackage.baselines;//[....]
                        //Check color status
                        let greenNum = 0;
                        redNum = 0;
                        yellowNum = 0;
                        _.forEach(baselines, function (base) {
                            if (base.status.toLowerCase().indexOf('up to date') !== -1) {
                                greenNum++;
                            } else if (base.status.toLowerCase().indexOf('location not') !== -1 || base.status.toLowerCase().indexOf('update not installed') !== -1) {
                                yellowNum++;
                            } else {
                                redNum++;
                            }
                        });
                        data.last_reported = lastPackage.reported_datetime;
                        data.data_compliant_icon = {
                            green: greenNum,
                            red: redNum,
                            yellow: yellowNum
                        };
                    }
                } else if (data.compliance === undefined) {
                    //List function undefined
                    context.log('=======START=====');
                    context.log(data);
                    context.log('======END======');
                } else {
                    data.last_reported = '';
                    data.data_compliant_icon = {
                        green: 0,
                        red: 0,
                        yellow: 0
                    };
                }

//List device not have data.compliance

            });

            //Merge them
            //context.log(changedFieldData)
            _.assignIn(allData, changedFieldData);
            //context.log(allData)
            //Save in Cosmos-- BE cONSIDER BEFORE SAVE ON db then open below comment
            //context.bindings.inputDocumentOut = allData;

        } else {
            const msg = 'Not found, wrong data input or data blank. Please, set key \'typeDocument\' in body. \n => \'typeDocument\' = ' + config.typeDocument;
            context.log('[404]-' + msg);
            context.res = {
                status: 404,
                body: msg
            };
        }
        context.done();
    } catch (err) {
        context.log(err); //For print console log
        //For Output console
        context.res = {
            status: 500,
            body: 'ERROR: ' + err
        };
    }
};

//Tested!