/*JUST FOR TESTING INPUT MANY DEVICES TO DB - TEST PERFORMANCE
 * IT WILL BE REMOVED LATER 
 * Clone from addNewAirlineDevice
 * Used compliancedemo2
 * Add new Airline device
 * Validated device is existed or not via device name
 * @method: POST
 * @params: codeAirline - an airline Code
 * @body  : device info - JSON Obj - compliance is a new array, it will be defined in function
 * @return: status  
 */
const uuid = require('uuid-v4');
const datetime = require('node-datetime');
const DocumentFactory = require('../api/DocumentDBUtils');
const documentdbclient = new DocumentFactory();
//var rp = require('request-promise');
module.exports = function (context, req) {
    context.log('--START CHECK AND ADDNEW DEVICE NAME ' + req.body.name + '--');
    try {
        const infoDevice = req.body;

        //Add new device
        //Auto generate uuid for device
        const deviceId = (infoDevice.id) ? infoDevice.id : uuid();
        infoDevice.id = deviceId;
        infoDevice.tml_id = deviceId.toUpperCase();
        //Check if data not has datetime create current time

        const timeCreateDoc = datetime.create().format('Y-m-d H:M:S');
        //Format datetime before store
        let currReportTime = '';

        //Update
        if (infoDevice.last_package_datetime) {
            currReportTime = datetime.create(infoDevice.last_package_datetime).format('Y-m-d H:M:S');
        }
        else {
            currReportTime = '';
            //infoDevice.last_package_datetime = timeCreateDoc
        }

        //Check if device has compliance - for eventHandleMessage
        const bodyComplianceArr = infoDevice.compliance;
        const baselineArr = [];
        if (infoDevice.compliance && infoDevice.compliance.length !== 0) {
            //_.each(bodyComplianceArr,function(c){
            const item = {
                'baselines': bodyComplianceArr,
                'reported_datetime': currReportTime
            };
            baselineArr.push(item);
            infoDevice.compliance = baselineArr;
        }

        context.log(infoDevice);

        //delete sent_from_queue field if has
        delete infoDevice.sent_from_queue;
        delete infoDevice.last_package_datetime;

        //set back datetime to time that create this doc
        infoDevice.device_created_date = timeCreateDoc;

        //deviceIn.push(infoDevice);

        //obj.status = "Device name: "+ req.body.name + " has been added. Airline "+ req.query.airlineCode;
        //context.log("Device name: "+ req.body.name + " has been added success. Airline "+ req.query.airlineCode)

        documentdbclient.addADeviceByAirlineCodeTestingSDK(infoDevice, function (err, responseData) {
            //context.log(responseData)
            if (err === null) {
                //Result
                context.res = {
                    status: 200,
                    contentType: 'application/json',
                    body: {content: 'Device name: ' + req.body.name + ' has been added success. Airline ' + req.query.airlineCode}
                };
                context.log('Device name: ' + req.body.name + ' has been added success. Airline ' + req.query.airlineCode);
            } else {
                context.res = {
                    status: 400,
                    body: err
                };
                context.log(err);
            }
        });


    } catch (err) {
        context.log(err); //For print console log
        //For Output console
        context.res = {
            status: 500,
            body: 'ERROR: ' + err
        };
    }
    context.done();
};

//Hiep Tested!